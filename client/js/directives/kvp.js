angular
  .module('app')
  .directive('kvp', function kvp() {
    return {
      templateUrl: 'views/templates/kvp.html',
      replace: true,
      transclude: false,
      scope: {
        'key': "=?",
        'value': "=?",
        'updateFn': "=?",
        'deleteFn': "=?"
      }
    };
  });

angular
  .module('app')
  .directive('group', function group() {
    return {
      templateUrl: 'views/templates/group.html',
      replace: true,
      transclude: false,
      scope: {
        'model': "=?",
        'class': "@",
        'update': "=?",
        'add': "=?",
        'delete': "=?"
      },
      link: function(scope, element, attrs){
        scope.add = 'add' in attrs;
      }
    };
  });

KvcCtrl.prototype.loadKvcs = function($scope, Group, $stateParams) {

  //scopeObj, cat, ix
  var getKvc = function(){

    var args = [].slice.call(arguments);
    var catId = _.isEmpty(args) ? $stateParams.groupId : args[1];
    var ix = _.isEmpty(args) ? $stateParams.groupId : args[2];


    Group.prototype$__findById__kvps({

    })

    Group.prototype$__get__kvcs({
      id: catId
    })
      .$promise
      .then(function(kvcs) {

      });
  };

  /**
   * @func _getKvps
   * @param scopeObj {Object}
   * @param cat {Object}
   * @param ix {Number}
   * @private
   */
  var _getKvps = function(scopeObj, catId, ix){

    Group.prototype$__get__kvps({
      id: catId
    }).$promise
      .then(function(kvps){

        _.map(kvps, function(kvp){
          kvp.name = kvp.key;
        });

        scopeObj[ix]
          .kvps = kvps;
      });
  };

  getKvc();

};

KvcCtrl.prototype.editKvcs = function($scope, Group, $stateParams) {

  Group.prototype$__updateById__groups({ id: $stateParams.groupId }, $scope.existingCategory)
    .$promise.
    then(function() {
      console.log('updated');
    });
};

KvcCtrl.prototype.deleteKvcs = function($scope, Group, $stateParams) {

  Group.deleteById({ id: $stateParams.groupId })
    .$promise
    .then(function() {
      console.log('deleted');
    });
};

KvcCtrl.prototype.addKvcs = function($scope, Group, $stateParams) {

  Group.deleteById({ id: $stateParams.groupId })
    .$promise
    .then(function() {
      console.log('added');
    });
};







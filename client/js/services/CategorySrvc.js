CategoryCtrl.prototype.loadCategories = function($scope, Group, $stateParams) {

  $scope.oneAtATime = false;

  $scope.status = {
    isFirstOpen: true,
    isFirstDisabled: false
  };

  var getGroupSubCategories = function(scopeObj, catId, ix){

    Group.prototype$__get__groups({
      id: catId
    })
      .$promise
      .then(function(groupSubCategories) {

        if (_.isUndefined(ix)) {
          $scope.GroupSubCategories = groupSubCategories;
          var newScopeObj = $scope.GroupSubCategories;
        } else {
          scopeObj[ix].GroupSubCategories = groupSubCategories;
          var newScopeObj = scopeObj[ix].GroupSubCategories;
        }

        _.forEach(groupSubCategories, function (groupSubCategory, groupSubCategoryIndex) {

          _getKvps(newScopeObj, groupSubCategory.id, groupSubCategoryIndex);
          getGroupSubCategories(newScopeObj, groupSubCategory.id, groupSubCategoryIndex);

        });

      });

  };

  var _getKvps = function(scopeObj, catId, ix){

    Group.prototype$__get__kvps({
      id: catId
    }).$promise
      .then(function(kvps){

        _.map(kvps, function(kvp){
          kvp.name = kvp.key;
        });

        scopeObj[ix]
          .kvps = kvps;
      });
  };

  getGroupSubCategories($scope.GroupSubCategories, $stateParams.groupId);

};

CategoryCtrl.prototype.editCategory = function($scope, Group, $stateParams) {

  Group.prototype$__updateById__groups({ id: $stateParams.groupId }, $scope.existingCategory)
    .$promise.
    then(function() {
      console.log('updated');
    });
};

CategoryCtrl.prototype.deleteCategory = function($scope, Group, $stateParams) {

  Group.deleteById({ id: $stateParams.groupId })
    .$promise
    .then(function() {
      console.log('deleted');
    });
};

CategoryCtrl.prototype.addCategory = function($scope, Group, $stateParams) {

  Group.deleteById({ id: $stateParams.groupId })
    .$promise
    .then(function() {
      console.log('added');
    });
};







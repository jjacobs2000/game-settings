
TabCtrl.prototype.initialize = function($scope, Group, $state, $sce) {

  $scope.go = function(state) {
    $state.go(state);
  };

  $scope.tabData = [];

  Group
    .find()
    .$promise
    .then(function (results) {

      var categories = _.filter(results, function (result) {
        return !_.has(result, 'groupId');
      });

      _.forEach(categories, function (category, ix) {

        $scope.existingCategory = category;

        var newGroupTemplate =
          '<div class="row">' +
          '<div class="column-lg-6 pull-left">' +
          '<input class="form-control" type="text" ng-model="existingCategory.name" value="' +
          category.name +
          '">' +
          '</div>' +
          '<div class="btn-group" role="group">' +
          '<button type="button" class="btn btn-primary" ng-click="updateFn"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></button>' +
          '<button type="button" class="btn btn-danger" ng-click="deleteFn"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>';

        if (categories.length - 1 === ix)
          newGroupTemplate += '<button type="button" class="btn btn-success" ng-click="addFn"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>'
        newGroupTemplate += '</div></div>';

        $scope.tabData.push(
          {
            heading: $sce.trustAsHtml(newGroupTemplate),
            route:   'group.' + category.name,
            params:  {
              groupId: category.id
            },
            options: {}
          }
        )
      });
    });

};

TabCtrl.prototype.editTab = function($scope, Group, $stateParams) {

  Group.prototype$updateAttributes({ id: $stateParams.groupId }, $scope.existingCategory)
    .$promise.
    then(function() {
      console.log('updated');
    });
};

TabCtrl.prototype.deleteTab = function($scope, Group, $stateParams) {

  Group.deleteById({ id: $stateParams.groupId })
    .$promise
    .then(function() {
      console.log('deleted');
    });
};

TabCtrl.prototype.addTab = function($scope, Group, $state, $sce) {

  $scope.newTab = '';

  var newGroupTemplate =
    '<div class="col-lg-6">' +
    '<div class="input-group">' +
    '<input class="form-control" type="text" ng-model="newTab.name">' +
    '<span class="input-group-btn">' +
    '<button type="button" class="btn btn-primary" ng-click="saveTab()"><span class="glyphicon glyphicon-save" aria-hidden="true"></span> Save</button>' +
    '</span>' +
    '</div>' +
    '</div>'

  $scope.tabData.push(
    {
      heading: $sce.trustAsHtml(newGroupTemplate),
      route:   '',
      params:  {
        groupId: ''
      },
      options: {}
    }
  );

  $scope.saveTab = function() {
    Group.create({name: $scope.newTab.name})
      .$promise
      .then(function () {
        $scope.tabData.pop();
      });
  }

};

var $stateProviderRef = null;
var $urlRouterProviderRef = null;

angular.module('app', [
  'lbServices',
  'ui.router',
  'ui.bootstrap',
  'ui.router.tabs',
  'ngSanitize'
])
  .run(['$rootScope', '$state', '$stateParams',
    function($rootScope, $state, $stateParams) {
      $rootScope.$state = $state;
      $rootScope.$stateParams = $stateParams;
    }
  ])
  .config(['$locationProvider', '$stateProvider', '$urlRouterProvider',
    function($locationProvider, $stateProvider, $urlRouterProvider) {

    $stateProvider
      .state('group', {
        url: '/',
        templateUrl: 'views/tab-menu.html',
        controller: 'TabCtrl'
      });

    $urlRouterProvider.deferIntercept();

    $urlRouterProvider.otherwise('/');

    $locationProvider.html5Mode({
      enabled: false
    });

    $stateProviderRef = $stateProvider;
    $urlRouterProviderRef = $urlRouterProvider;

  }])
  .run(['Group','$rootScope', '$urlRouter', '$state', function(Group, $rootScope, $urlRouter, $state){

    Group
      .find()
      .$promise
      .then(function (results) {

        var categories = _.filter(results, function (result) {
          return !_.has(result, 'groupId');
        });

        _.forEach(categories, function (category) {

          var getExistingState = $state.get('group.' + category.name)

          if(getExistingState !== null){
            return;
          }

          var state = {
              abstract: false,
              url: '^/' + category.name + '/:groupId',
              templateUrl: 'views/sub-category.html',
              controller: 'TabCtrl'
          };

          $stateProviderRef.state('group.' + category.name, state);
        });


        $urlRouter.sync();
        $urlRouter.listen();

      });
  }])

var TabCtrl = function($scope, Group, $state, $stateParams, $sce){

  $scope.initialize = function() {
    this.initialize($scope, Group, $state, $sce);
  }.bind(this);

  $scope.initialize();

  $scope.addTab = function() {
    this.addTab($scope, Group, $state, $stateParams, $sce);
    $scope.initialize();
  }.bind(this);

  $scope.deleteTab = function(){
    this.deleteTab(Group, $stateParams);
    $scope.initialize();
  }.bind(this);

  $scope.editTab = function(){
    this.editTab(Group, $stateParams);
    $scope.initialize();
  }.bind(this);

};

TabCtrl.$inject = ['$scope', 'Group', '$state', '$stateParams', '$sce'];

angular
  .module('app').controller('TabCtrl', TabCtrl);

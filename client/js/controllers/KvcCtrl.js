var KvcCtrl = function($scope, Group, $stateParams){

  var loadKvcs = function(){
    this.loadKvcs($scope, Group, $stateParams);
  }.bind(this);

  loadKvcs();

  $scope.deleteKvc = function(groupId){
    this.deleteKvc(groupId, $scope, Group, $stateParams);
    loadKvcs();
  }.bind(this);

  $scope.addKvc = function(groupId){
    this.addKvc(groupId, $scope, Group, $stateParams);
    loadKvcs();
  }.bind(this);

  $scope.editKvc = function(groupId){
    this.editKvc(groupId, $scope, Group, $stateParams);
    loadKvcs();
  }.bind(this);

};

KvcCtrl.$inject = ['$scope', 'Group', '$stateParams'];

angular
  .module('app').controller('KvcCtrl', KvcCtrl);

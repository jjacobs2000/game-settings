var CategoryCtrl = function($scope, Group, $stateParams){

  var loadCategories = function(){
    this.loadCategories($scope, Group, $stateParams);
  }.bind(this);

  loadCategories();

  $scope.deleteCategory = function(){
    this.deleteCategory($scope, Group, $stateParams);
    loadCategories();
  }.bind(this);

  $scope.addCategory = function(){
    this.addCategory($scope, Group, $stateParams);
    loadCategories();
  }.bind(this);

  $scope.editCategory = function(){
    this.editCategory($scope, Group, $stateParams);
    loadCategories();
  }.bind(this);
};

CategoryCtrl.$inject = ['$scope', 'Group', '$stateParams'];

angular
  .module('app').controller('CategoryCtrl', CategoryCtrl);

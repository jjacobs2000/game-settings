# [Unity Engineer Test](./INSTRUCTIONS.md) Game Settings API

## API
This project utilizes:
[Strongloop's](https://strongloop.com/) [loopback API framework](https://strongloop.com/node-js/loopback-framework/).

I felt this framework would allow me to quickly fulfill the requirements of this test due to:

 - Easy noSQL document modeling ORM
 - Swagger explorer
 - AngularJS SDK

## Idea

The idea was to allow a user to create a Settings Screen for Game options like the following:

![World of Tanks Strategy Game Settings](http://www.worldoftanksguide.com/images/graphic-settings.jpg)

## Front End

Please note the UI is not functional and the layout leaves much to be desired.  Creating a recursive group editing UI was a bit ambitious in the time I had.

 - [AngularJS](https://angularjs.org/)
 - [Bootstrap](http://getbootstrap.com/)
 - [Angular-UI-Router](https://github.com/angular-ui/ui-router/wiki)

# Concept Mockup

[Wireframe concept mockup viewable here](https://docs.google.com/drawings/d/1UKB7rIjEZfQv4rXFagVzmWZnee8N1Fpc06B-bY58w1c/edit?usp=sharing)

# Hosted Demo - Coming Soon on IBM Bluemix

- Application: Setting up on IBM Bluemix
- Swagger Explorer: Setting up on IBM Bluemix

# Install

Running a loopback application locally requires:

 - [node.js](https://nodejs.org/download/).
 - [MongoDB](https://www.mongodb.org).

1. Clone this repo: `git clone git@bitbucket.org:jjacobs2000/game-settings.git`
2. Install global node modules.
```
    npm install -g bower
    npm install -g strongloop
```
3. Install application modules:
```
    cd game-settings
    npm install
    bower install
```
4. Import sample data into local mongoDB (`mongod` must be running):
```
    cd game-settings
    mongoexport --db unity
```

# Run App

1. In the application `game-settings` directory:

```
node .
```
2. View app: http://localhost:3000
3. View swagger API explorer: http://localhost:3000/explorer

If you loaded up the demo data you should see two tabs, _Game_ and _Video_.
You'll have to click on the _Game_ tab to see the content since it doesn't come up initially.




module.exports = function(Kvp) {

  Kvp.validate('value', customValidator, {message: 'Values can only be simple primitive types (string, number, boolean, and null)'});

  function customValidator(err) {
    switch (typeof this.value) {
      case 'string':
      case 'number':
      case 'boolean':
      case 'null': break;
      default: err();
    };
  };

  Kvp.validatesFormatOf('key', {with: /^[a-z0-9.-]+$/, message: 'Keys can be arbitrary strings (within character set [a-z0-9.-])'});
};
